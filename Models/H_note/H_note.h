#ifndef PET_SHOP_H_NOTE_H
#define PET_SHOP_H_NOTE_H

#include <vector>
#include <string>
#include <ctime>
#include <Item/Item.h>
#include <User/User.h>

using std::string;
using std::vector;
using std::time_t;

struct H_note {

    Item *item{};    // купленный товар
    User *user{};    // покупатель
    time_t time{};   // время покупки

};

using HistoryList = vector<H_note*>;

#endif //PET_SHOP_H_NOTE_H

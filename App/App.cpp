#include "App.h"

void App::_init() {
    // устаналиваем начальный экран
    m_current_screen = static_cast<MapScreens *>(m_store.getState().screens_map)->at(IdScreens::AUTH);

    m_is_run = true;
}

App::App()
        : m_store(Store::createStore(
        State{
                // TODO: настраиваем начальное состояние нашего приложения

                // устанавливаем название программы
                .app_name = "Pet Shop",

                // устанавливаем экраны программы
                // создаём в памяти словарь экранов
                // ключом будет являться Id экрана
                // а значением указатель на экран
                // каждый экран является классом - синглтоном
                // по этому вызываем их статические методы
                // которые возвращают нам указатели на объекты
                .screens_map = new map<int, InterfaceScreen *>{
                        {IdScreens::AUTH,           Auth::createScreen()},
                        {IdScreens::MAIN,           Main::createScreen()}
                }
        }))

{
    _init();
}

App &App::createApp() {
    static App app{};

    return app;
}

int App::start() {
    int code_return = 0;

    while (m_is_run) {
        // запускаем главный экран нашего приложения
        code_return = m_current_screen->start();

        // проверяем код с которым закончил работу экран
        if (code_return != 0) {
            std::cerr
                    << "The screen was closed with error! App.cpp - start()"
                    << std::endl;

            return code_return;
        }

        // если всё хорошо, то запускаем обработку действий
        // после закрытия экрана
        _startTransactionScreen();
    }

    return code_return;
}

bool App::_pushStackCurrentScreen() {
    // если имеется текущий скрин
    // то добавляем указатель на него
    // в стэк
    if (m_current_screen) {
        m_stack_screens.push(m_current_screen);
        return true;
    }

    return false;
}

bool App::_pullStackCurrentScreen() {
    // если стэк пустой
    // то возвращаем false, иначе
    // восстанавливаем последний скрин
    if (m_stack_screens.empty()) {
        return false;
    }

    // возвращаем указатель на последний экран
    m_current_screen = m_stack_screens.top();

    // удаляем его из стэка
    m_stack_screens.pop();

    return true;
}

App::~App() {
    // получаем словарь экранов
    MapScreens &mapScreens = *static_cast<MapScreens *>(m_store.getState().screens_map);

    // бежим по экранам и удаляем их объекты из store
    delete dynamic_cast<Auth *>(mapScreens.at(IdScreens::AUTH));
    delete dynamic_cast<Main *>(mapScreens.at(IdScreens::MAIN));

    // удаляем массив экранов по указателю
    delete &mapScreens;
}

void App::_startTransactionScreen() {
    // проверяем, нужно ли запускать другой экран
    if (m_store.getState().intent_next_screen) {
        // если нужно, чекаем, слеудет ли сохранять текущий экран
        // в стэк
        if (m_store.getState().is_push_stack) {
            // если да, то сохраняем текущий экран в стеке
            _pushStackCurrentScreen();

            // теперь обнуляем состояние помещения в стек
            m_store._reducer(Action{
                    ActionTypes::CLEAR_PUSH_STACK_SCREEN
            });
        }

        // устанавливаем новый экран
        m_current_screen = static_cast<InterfaceScreen *>(m_store.getState().intent_next_screen);

        // обнуляем указатель на следующий экран
        m_store._reducer(Action{
                ActionTypes::CLEAR_INTENT_NEXT_SCREEN
        });
    } else {
        // если не нужно запускать другой экран
        // пытаемся получить прошлый экран из стэка
        if (bool err = _pullStackCurrentScreen(); !err) {
            // если стэк пуст, значит закрылся последний экран программы
            m_is_run = false;
        }
    }
}

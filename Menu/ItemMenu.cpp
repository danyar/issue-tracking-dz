#include "ItemMenu.h"

ItemMenu::ItemMenu(string item_name, FuncExec func_exec)
        : m_item_name(std::move(item_name)), m_func_exec(func_exec) {}

string ItemMenu::getItemName() {
    return m_item_name;
}

int ItemMenu::exec(int selected) {
    return m_func_exec(selected);
}

ItemMenu::ItemMenu(const ItemMenu & other) {
    if (this != &other) {
        this->m_item_name = other.m_item_name;
        this->m_func_exec = other.m_func_exec;
    }
}

ItemMenu &ItemMenu::operator=(const ItemMenu & other) {
    if (this == &other) return *this;

    this->m_item_name = other.m_item_name;
    this->m_func_exec = other.m_func_exec;

    return *this;
}

std::ostream &operator<<(std::ostream &out, const ItemMenu &itemMenu) {
    out << itemMenu.m_item_name;
    return out;
}

int ItemMenu::operator()(int selected) {
    return exec(selected);
}


#include "Menu.h"

Menu::Menu(string title, ItemList items)
        : m_title(std::move(title)), m_items(std::move(items)) {}

void Menu::selectItem(int selected) {
    if (selected > 0 && selected <= m_items.size()) {
        m_selected = selected - 1;
    } else {
        m_selected = -1;
    }
}

int Menu::execSelected() {
    if (m_selected == -1) {
        return -1;
    }

    return m_items.at(m_selected)(m_selected);
}

std::ostream &operator<<(std::ostream &out, Menu &menu) {
    out << menu.m_title;
    out << "\n------------------\n";

    int index{0};

    for(const auto &item : menu.m_items) {
        out << ++index << " --> " << item << '\n';
    }
    out << std::endl;

    return out;
}

std::istream &operator>>(std::istream &in, Menu &menu) {
    bool is_loop{true};
    int selected{};

    while (is_loop) {
        is_loop = false;

        cout << "\nInput item number --> ";

        in >> selected;

        try {
            isOutRange(selected, 0, menu.m_items.size());
        }
        catch (InvalidItemNumber & invalid_item_number) {
            cout << invalid_item_number.what();
            is_loop = true;
            continue;
        }

        menu.selectItem(selected);
    }

    cout << std::endl;

    return in;
}

int Menu::operator()() {
    return execSelected();
}

void Menu::addItemByIndex(const ItemMenu& itemMenu, int index) {
    if (index < m_items.size()) {
        m_items.insert(m_items.begin() + index, itemMenu);
    }
}

void isOutRange(int number, int left_range, int right_range) {

    if (!(number > left_range && number <= right_range)) {
        throw InvalidItemNumber();
    }
}

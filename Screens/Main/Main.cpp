#include "Main.h"

Main *Main::createScreen() {
    static auto *main_screen = new Main();

    return main_screen;
}

int Main::start() {
    m_store = Store::getStore();

    Menu &menu = *createMainMenu();

    m_is_running = true;

    while (m_is_running) {

        system("cls");

        m_store = Store::getStore();

        render();

        print(m_store->getState().items_list);

        cout << menu;

        cin >> menu;

        menu();
    }

    delete &menu;

    return 0;
}

Menu *Main::createMainMenu() {

    Main *main = Main::createScreen();

    Menu *menu = new Menu("Main menu", ItemList{
            ItemMenu("Search by name", [main](int index) -> int {
                main->search();
                return index;
            }),
            ItemMenu("Sort by price (descending)", [main](int index) -> int {
                main->sortDescending();
                return index;
            }),
            ItemMenu("Sort by price (ascending)", [main](int index) -> int {
                main->sortAscending();
                return index;
            }),
            ItemMenu("Sign out", [main](int index) -> int {
                main->quit();
                main->m_is_running = false;
                return index;
            }),
    });

    if (m_store->getState().current_user->levelAccess == User::LevelsOfAccess::USER) {
        menu->addItemByIndex(ItemMenu("Buy", [main](int index) -> int {
            main->buy();
            return index;
        }), 0);
        menu->addItemByIndex(ItemMenu("Open the order history", [main](int index) -> int {
            main->openHistoryForUser();
            return index;
        }), 1);
    }

    if (m_store->getState().current_user->levelAccess == User::LevelsOfAccess::ADMIN) {
        menu->addItemByIndex(ItemMenu("Add new item", [main](int index) -> int {
            main->add();
            return index;
        }), 0);
        menu->addItemByIndex(ItemMenu("Remove item", [main](int index) -> int {
            main->remove();
            return index;
        }), 1);
        menu->addItemByIndex(ItemMenu("Edit item", [main](int index) -> int {
            main->edit();
            return index;
        }), 2);
        menu->addItemByIndex(ItemMenu("Open the order history", [main](int index) -> int {
            main->openHistoryForAdmin();
            return index;
        }), 3);
    }

    return menu;
}

void Main::add() {

    if (cin.fail()) {
        cin.clear();
        cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }

    string name{};
    cout << "Item's name -> ";
    cin.get();
    getline(cin, name);
    cout << endl;

    string description{};
    cout << "Item's description -> ";
    getline(cin, description);
    cout << endl;

    float price = getEnteredNumFloat("Item's price -> ");

    auto new_item = new Item{
        name,
        description,
        price,
        Item::current_item_id++
    };

    m_store->_reducer(Action{
        ActionTypes::ADD_NEW_ITEM,
        static_cast<void*>(new_item)
    });

    cout << "\n\nThe item was added successful!\n";
}

void Main::edit() {
    auto item = new Item{};
    bool is_running{};

    State state = m_store->getState();
    Item edited_item{};

    item->id = getEnteredNumInt(
            "Input item's id -> ",
            [state](int id) -> bool {
                for (auto *pItem : state.items_list) {
                    if (pItem->id == id) {
                        return true;
                    }
                }
                return false;
            }
    );

    Menu menu("", ItemList{
            ItemMenu("Edit item's name", [item, this](int index) -> int {

                cout << "Input another item's name -> ";
                cin.get();
                getline(cin, item->name);
                cout << endl;

                m_store->_reducer(Action{
                    ActionTypes::EDIT_ITEM_NAME,
                    static_cast<void *>(item)
                });

                //cout << "\n\nНазание товара успешно изменено!\n";

                return index;
            }),
            ItemMenu("Edit item's description", [item, this](int index) -> int {

                cout << "Input another item's description -> ";
                cin.get();
                getline(cin, item->description);
                cout << endl;

                m_store->_reducer(Action{
                        ActionTypes::EDIT_ITEM_DESCRIPTION,
                        static_cast<void *>(item)
                });

                //cout << "\n\nОписание товара успешно изменено!\n";

                return index;
            }),
            ItemMenu("Edit item's price", [item, this](int index) -> int {

                item->price = getEnteredNumFloat("Input another item's price -> ");

                m_store->_reducer(Action{
                        ActionTypes::EDIT_ITEM_PRICE,
                        static_cast<void *>(item)
                });

                //cout << "\n\nСтоимость товара успешно изменена!\n";

                return index;
            }),
            ItemMenu("Back", [&is_running](int index) -> int {
                is_running = false;
                return index;
            }),
    });

    is_running = true;

    while (is_running) {

        system("cls");

        edited_item = *m_store->getState().items_list.at(item->id - 1);

        cout << edited_item.id << ". "
             << edited_item.name << "\n\n"
             << edited_item.description << "\n\n"
             << "Price: " << edited_item.price << " rubles\n\n";

        cout << menu;

        cin >> menu;

        menu();
    }
}

void Main::search() {

    string name{};
    cout << "Item's name -> ";
    cin.get();
    getline(cin, name);
    cout << endl;

    bool is_running{}, is_searched{};

    Menu menu("", ItemList{
            ItemMenu("Back", [&is_running](int index) -> int {
                is_running = false;
                return index;
            }),
    });

    is_running = true;

    while (is_running) {

        system("cls");

        for (const auto & item : m_store->getState().items_list) {
            if (item->name == name) {
                cout << "The searched item's info:\n\n"
                     << '\t' << item->name << "\n\n"
                     << '\t' << item->description << "\n\n"
                     << '\t' << "Price: " << item->price << " rubles\n";
                is_searched = true;
            }
        }

        if (!is_searched) {
            cout << "\n\nThe item wasn't searched!\n\n";
        }

        cout << menu;

        cin >> menu;

        menu();
    }
}

void Main::print(const ItemsList& item_list) {

    //system("clear");

    for (const auto & item : item_list) {
        cout << "\n======================================\n"
             << item->id << ". "
             << item->name << ";\n"
             << item->description << ";\n"
             << "Price: " << item->price << " rubles;\n";
    }
    cout << "\n======================================\n\n\n";

}

void Main::render() const
{
    cout << "Welcome to main menu the Pet Shop app, "
    << m_store->getState().current_user->name << ".\n\n";
}

void Main::remove()
{
    State state = m_store->getState();
    int deleteIndex{-1};
    int deleteId = getEnteredNumInt("Item's id -> ");

    for (int i = 0; i < state.items_list.size(); ++i) {
        if (state.items_list.at(i)->id == deleteId)
        {
            deleteIndex = i;
        }
    }

    if (deleteIndex == -1) {
        cout << "\n\nInvalid id!\n\n";
        return;
    }

    m_store->_reducer(Action{
            ActionTypes::DELETE_ITEM,
            static_cast<void*>(&deleteIndex)
    });

    cout << "\n\nThe item was removed successful!!\n";
}

void Main::buy()
{
    State state = m_store->getState();
    int buyIndex{-1};
    int buyId = getEnteredNumInt("Item's id -> ");

    for (int i = 0; i < state.items_list.size(); ++i) {
        if (state.items_list.at(i)->id == buyId)
        {
            buyIndex = i;
        }
    }

    system("cls");

    if (buyIndex == -1) {
        cout << "\n\nInvalid id!\n\n";
    }
    else {
        cout << "\n\nThe item was bought successful!!\n\n";
        cout << "\n#######################################\n"
             << "OOO DanYar\n"
             << m_store->getState().items_list[buyIndex]->id << ". "
             << m_store->getState().items_list[buyIndex]->name
             << "\nCount: 1"
             << "\nPrice: " << (m_store->getState().items_list[buyIndex]->price) -
                               (m_store->getState().items_list[buyIndex]->price/100*20)
             << "\nVAT 20%: " << m_store->getState().items_list[buyIndex]->price/100*20
             << "\nPayment method: Full payment"
             << "\nObject of payment: item"
             << "\n\nResult: " << m_store->getState().items_list[buyIndex]->price
             << "\n  Non-cash: " << m_store->getState().items_list[buyIndex]->price
             << "\n#######################################\n\n";

        m_store->_reducer(Action{
            ActionTypes::ADD_H_NOTE,
            static_cast<void*>(&buyIndex)
        });

        m_store->_reducer(Action{
                ActionTypes::DELETE_ITEM,
                static_cast<void*>(&buyIndex)
        });
    }

    cout << "Press Enter to get back to main menu.";
    cin.get();
    cin.ignore();
}

void Main::sortAscending()
{
    for(int i = 0; i < m_store->getState().items_list.size() - 1; i++)
    {
        for(int j = 0; j < m_store->getState().items_list.size() - i - 1; j++)
        {
            if(m_store->getState().items_list[j]->price > m_store->getState().items_list[j + 1]->price)
            {
                Item temp;
                std::swap(*m_store->getState().items_list[j],
                          *m_store->getState().items_list[j+1]);
            }

        }
    }
}

void Main::sortDescending()
{
    for(int i = 0; i < m_store->getState().items_list.size() - 1; i++)
    {
        for(int j = 0; j < m_store->getState().items_list.size() - i - 1; j++)
        {
            if(m_store->getState().items_list[j]->price < m_store->getState().items_list[j + 1]->price)
            {
                Item temp;
                std::swap(*m_store->getState().items_list[j],
                          *m_store->getState().items_list[j+1]);
            }

        }
    }
}

void Main::quit() {

    m_store->_reducer(Action{
        ActionTypes::CLEAR_CURRENT_USER
    });

    m_store->_reducer(Action{
            ActionTypes::SET_INTENT_NEXT_SCREEN,
            static_cast<MapScreens *>(m_store->getState().screens_map)->at(IdScreens::AUTH)
    });

}

void Main::openHistoryForAdmin() {

    system("cls");

    if (!m_store->getState().history_list.empty()) {

        tm *time_info{};

        for(auto item : m_store->getState().history_list) {
            cout << "\n======================================\n";

            cout << "Item's name: " << item->item->name << "\n"
                 << "Item's price: " << item->item->price << " rubles\n\n";

            cout << "Bought: " << item->user->name
                              << item->user->surname << "\n\n";

            time_info = localtime(&item->time);
            cout << "Purchase time: " << asctime(time_info);

            cout << "\n======================================\n\n";
        }

    }
    else {
        cout << "History list is empty!\n\n";
    }

    cout << "Press Enter to get back to main menu...";
    cin.get();
    cin.ignore();

}

void Main::openHistoryForUser() {

    system("cls");

    if (!m_store->getState().history_list.empty()) {

        tm *time_info{};

        for(auto item : m_store->getState().history_list) {

            if (item->user->name == m_store->getState().current_user->name) {

                cout << "\n======================================\n";

                cout << "Item's name: " << item->item->name << "\n"
                     << "Item's price: " << item->item->price << " rubles\n\n";

                cout << "Bought: " << item->user->name
                     << item->user->surname << "\n\n";

                time_info = localtime(&item->time);
                cout << "Purchase time: " << asctime(time_info);

                cout << "\n======================================\n\n";

            }
        }

    }
    else {
        cout << "History list is empty!\n\n";
    }

    cout << "Press Enter to get back to main menu...";
    cin.get();
    cin.ignore();

}

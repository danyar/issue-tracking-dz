#ifndef PET_SHOP_MAIN_H
#define PET_SHOP_MAIN_H

// Main - класс синглтон, управляющий логикой экрана главного меню

#include <InterfaceScreen.h>

class Main : public InterfaceScreen {
public:

    Main(const Main &) = delete;

    Main &operator=(const Main &) = delete;

    static Main *createScreen();

    void render() const override;

    int start() override;

    Menu *createMainMenu();

    void add();

    void remove();

    void edit();

    void buy();

    void search();

    void sortAscending();

    void sortDescending();

    void print(const ItemsList& item_list);

    void openHistoryForAdmin();

    void openHistoryForUser();

    void quit();

protected:
    Store *m_store{}; // указатель на объект хранилища

    bool m_is_running{};

    Main() = default;

};


#endif //PET_SHOP_MAIN_H

#ifndef SCHOOL_SYSTEM_INTERFACE_SCREEN_H
#define SCHOOL_SYSTEM_INTERFACE_SCREEN_H

#include <iostream>
#include <string>
#include <stack>
#include <map>
#include <Store.h>
#include <Menu.h>
#include <consoleUtils.h>

using std::cout;
using std::string;
using std::endl;

enum IdScreens {
    AUTH,
    MAIN
};

class InterfaceScreen {
public:
    virtual int start() = 0;

    virtual void render() const = 0;
};

using MapScreens = std::map<IdScreens, InterfaceScreen *>;

using StackScreens = std::stack<InterfaceScreen *>;

bool validateEnter(const string &str);

#endif //SCHOOL_SYSTEM_INTERFACE_SCREEN_H

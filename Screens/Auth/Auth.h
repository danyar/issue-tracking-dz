#ifndef PET_SHOP_AUTH_H
#define PET_SHOP_AUTH_H

#include <InterfaceScreen.h>
#include <User/User.h>


class Auth : public InterfaceScreen {
public:
    Auth(const Auth &) = delete;

    Auth & operator=(const Auth &) = delete;

    static Auth *createScreen();

    int start() override;

    void render() const override;

    Menu *createAuthMenu();

    void auth();

    void createAccount();

    static Auth *getAuthScreen();

private:

    Store *m_store{};

    static Auth *m_auth;

    Auth() = default;
};

void checkRepeatedPassword(const string& password, const string& repeated_password);

void checkUserInData(const UsersList & user_list, const string & login, const string & password);

void checkUserLogin(const UsersList & users_list, const string & login);


#endif //PET_SHOP_AUTH_H

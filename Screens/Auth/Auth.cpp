#include "Auth.h"

Auth *Auth::m_auth{};

Auth *Auth::createScreen() {
    static Auth *auth = new Auth;

    m_auth = auth;

    return auth;
}

int Auth::start() {
    m_store = Store::getStore();

    render();

    Menu &menu = *createAuthMenu();

    cout << menu;

    cin >> menu;

    menu();

    delete &menu;

    return 0;
}

void Auth::render() const {
    system("cls");

    cout << "Welcome! Sign in or create new account.\n"
         << "============================================\n\n"
         << endl;
}

Menu *Auth::createAuthMenu() {

    Auth *auth_screen = Auth::createScreen();

    Menu *menu = new Menu("", ItemList{
            ItemMenu("Sign in", [auth_screen](int index) -> int {
                auth_screen->auth();
                return index;
            }),
            ItemMenu("Create new account", [auth_screen](int index) -> int {
                auth_screen->createAccount();
                return index;
            }),
            ItemMenu("Exit", [](int index) -> int {
                return index;
            })
    });

    return menu;
}

void Auth::auth() {

    string login{};
    string password{};

    while (true) {

        cout << "Enter your login details:\n";

        login = getEnteredString("Login -> ", validateEnter);
        password = getEnteredString("Password -> ", validateEnter);

        try {
            checkUserInData(m_store->getState().users_list, login, password);
        }
        catch (InvalidUser & invalid_user) {
            cout << invalid_user.what();
            continue;
        }

        for (User *user : m_store->getState().users_list) {
            if ((user->login == login) && (user->password == password)) {

                m_store->_reducer(Action{
                        ActionTypes::SET_CURRENT_USER,
                        static_cast<void *>(user)
                });

                m_store->_reducer(Action{
                    ActionTypes::SET_INTENT_NEXT_SCREEN,
                    static_cast<MapScreens *>(m_store->getState().screens_map)->at(IdScreens::MAIN)
                });

                return;
            }
        }

        cout << "\n\nInvalid login or password! Try again!\n\n";
    }
}

void Auth::createAccount() {

    cout << endl;

    auto *user = new User{};

    user->name = getEnteredString("Your first name -> ", validateEnter);

    user->surname = getEnteredString("Your surname -> ", validateEnter);

    bool loop = true;

    while (loop) {
        user->login = getEnteredString("Login -> ", validateEnter);

        try {
            checkUserLogin(m_store->getState().users_list, user->login);
        }
        catch (InvalidUserLogin & invalid_user_login) {
            cout << invalid_user_login.what();
            continue;
        }

        loop = false;
    }

    string repeat_password{};
    loop = true;

    while (loop) {

        user->password = getEnteredString("Password -> ", validateEnter);
        repeat_password = getEnteredString("Repeat the password -> ", validateEnter);

        try {
            checkRepeatedPassword(user->password, repeat_password);
        }
        catch (InvalidRegPassword & invalid_reg_password) {
            cout << invalid_reg_password.what();
            continue;
        }

        loop = false;
    }

    user->id = User::current_user_id++;

    m_store->_reducer(Action{
            ActionTypes::SET_CURRENT_USER,
            static_cast<void *>(user)
    });

    m_store->_reducer(Action{
        ActionTypes::ADD_NEW_USER,
        static_cast<void *>(user)
    });

    m_store->_reducer(Action{
            ActionTypes::SET_INTENT_NEXT_SCREEN,
            static_cast<MapScreens *>(m_store->getState().screens_map)->at(IdScreens::MAIN)
    });
}

Auth *Auth::getAuthScreen() {
    return m_auth;
}

void checkRepeatedPassword(const string & password, const string & repeated_password) {

    if (password != repeated_password)
        throw InvalidRegPassword();

}

void checkUserInData(const UsersList & user_list, const string & login, const string & password) {

    bool check{};

    for (User *user : user_list) {
        if ((user->login == login) && (user->password == password)) {
            check = true;
            break;
        }
    }

    if (!check) throw InvalidUser();

}

void checkUserLogin(const UsersList & users_list, const string & login) {

    bool check{};

    for (User *user : users_list) {
        if (user->login == login) {
            check = true;
            break;
        }
    }

    if (check) throw InvalidUserLogin();

}
